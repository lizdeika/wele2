/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lt.jopapa.android.wele2;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
//import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import java.io.ByteArrayOutputStream;
//import java.io.IOException;
import java.io.InputStream;

import lt.jopapa.android.wele2.R;

public class Wele2Helper {
	private static final String TAG = "Wele2Helper";

	/**
	 * {@link StatusLine} HTTP status code when no server error has occurred.
	 */
	private static final int HTTP_STATUS_OK = 200;

	/**
	 * Shared buffer used by {@link #getUrlContent(String)} when reading results
	 * from an API request.
	 */
	private static byte[] sBuffer = new byte[512];

	/**
	 * User-agent string to use when making requests. Should be filled using
	 * {@link #prepareUserAgent(Context)} before making any other calls.
	 */
	private static String sUserAgent = null;

	/**
	 * Thrown when there were problems contacting the remote API server, either
	 * because of a network error, or the server returned a bad status code.
	 */
	public static class ApiException extends Exception {
		static final long serialVersionUID = -1L;

		public ApiException(String detailMessage, Throwable throwable) {
			super(detailMessage, throwable);
		}

		public ApiException(String detailMessage) {
			super(detailMessage);
		}
	}

	/**
	 * Prepare the internal User-Agent string for use. This requires a
	 * {@link Context} to pull the package name and version number for this
	 * application.
	 */
	public static void prepareUserAgent(Context context) {
		try {
			// Read package name and version number from manifest
			PackageManager manager = context.getPackageManager();
			PackageInfo info = manager.getPackageInfo(context.getPackageName(),
					0);
			sUserAgent = String.format(
					context.getString(R.string.template_user_agent),
					info.packageName, info.versionName);

			// } catch(NameNotFoundException e) {
		} catch (Exception e) {
			Log.e(TAG, "Nėra paketo informacijos PackageManager'yje", e);
		}
	}

	/**
	 * Read and return the content for a specific Wiktionary page. This makes a
	 * lightweight API call, and trims out just the page content returned.
	 * Because this call blocks until results are available, it should not be
	 * run from a UI thread.
	 * 
	 * @return Exact content of page.
	 * @throws ApiException
	 *             If any connection or server error occurs.
	 * @throws ParseException
	 *             If there are problems parsing the response.
	 */
	public static String getPageContent(String url) throws ApiException {
		String content = getUrlContent(url);
//		String content =
//		"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\">  <head>    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />   <meta name=\"Keywords\" content=\"Tele2, savitarna, Mano Tele2\">    <meta name=\"Description\" content=\"Tele2 savitarnos svetainė\">     <meta name=\"robots\" content=\"index, follow\" />     <meta name=\"robots\" content=\"noodp\" />      <title>Savitarna Numerio informacija</title>      <link href=\"http://m.manotele2.lt/static/mobcare/LT-LT/style/mobcare_content.css?noCache=1\" rel=\"stylesheet\" type=\"text/css\" />     <link href=\"http://m.manotele2.lt/static/mobcare/LT-LT/style/mobcare_general.css?noCache=1\" rel=\"stylesheet\" type=\"text/css\" />      <div id=\"mainContainer\">      <div class=\"mainDiv\">        <div class=\"topMeniu_1_level\">        <table>         <tbody>          <tr>           <td class=\"item_active\"><strong><a href=\"#\"><a href=\"http://m.manotele2.lt\">Mano TELE2</a></a></strong></td>           <td class=\"divider\"><img alt=\"\" src=\"http://m.manotele2.lt/static/mobcare/LT-LT/spc.gif\"/></td>           <td class=\"item_normal\"></td>          </tr>         </tbody>        </table>       </div>         <div class=\"topMeniu_2_level\">        <div class=\"mainPadding\">         &nbsp; <a href=\"http://m.manotele2.lt/main/\" >Mano informacija</a>         </li>         &nbsp; <a href=\"http://m.manotele2.lt/invoice/\" >Sąskaita</a>         </li>         <div style=\"clear:both; height:0px;\"><img src=\"http://m.manotele2.lt/static/mobcare/LT-LT/spc.gif\" alt=\"\" /></div>        </div>       </div>        <div class=\"contentContainer\">        <div class=\"topMeniu_3_level\">         <div class=\"topMeniu_3_bottomGrad\">          <div class=\"topMeniu_3_container\">           <div class=\"item_normal bottomBorder\"><b>Numeris:</b> 69953474</div><div class=\"item_normal bottomBorder\"><b>Mokėjimo planas:</b> Bronza</div><div class=\"item_normal bottomBorder\"><b>Kredito limitas:</b> 200</div><div class=\"item_normal bottomBorder\"><b>Einamojo mėn. sąskaita:</b> 4.62 </div><div class=\"item_normal bottomBorder\"><b>Mobilusis internetas (GPRS, EDGE, 3G)             :</b> 8.96&nbsp;MB</div>          </div>          </div>        </div>       </div>        <div class=\"bottom_separator\"></div>       <div class=\"link_more\">        <div class=\"link_more\"><img src=\"http://m.manotele2.lt/static/mobcare/LT-LT/link_more.gif\" alt=\"\" class=\"link_more_image\"/><a href=\"http://m.tele2.lt\"><b>TELE2 informacija - m.tele2.lt</b></a></div>       </div>       <div class=\"logo\">        <div class=\"logo_1\">Tele2</div>        <div class=\"logo_2\"><a href=\"http://m.tele2.lt/\" title=\"Tele2\"><img src=\"http://m.manotele2.lt/static/mobcare/LT-LT/tele2_logo.jpg\" alt=\"Tele2\" /></a></div>        <div style=\"clear:both; height:0px;\"><img src=\"http://m.manotele2.lt/static/mobcare/LT-LT/spc.gif\" alt=\"\" /></div>       </div>      </div>     </div>     </body>     </html>";
		return content;
	}

	/**
	 * Pull the raw text content of the given URL. This call blocks until the
	 * operation has completed, and is synchronized because it uses a shared
	 * buffer {@link #sBuffer}.
	 * 
	 * @param url
	 *            The exact URL to request.
	 * @return The raw content returned by the server.
	 * @throws ApiException
	 *             If any connection or server error occurs.
	 */
	protected static synchronized String getUrlContent(String url)
			throws ApiException {
		if (sUserAgent == null) {
			throw new ApiException("User-Agent neparuoštas");
		}

		try {
			// Create client and set our specific user-agent string
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
			request.setHeader("User-Agent", sUserAgent);

			HttpResponse response = client.execute(request);

			// Check if server response is valid
			StatusLine status = response.getStatusLine();
			if (status.getStatusCode() != HTTP_STATUS_OK) {
				throw new ApiException("Blogas atsakymas iš serverio: "
						+ status.toString());
			}

			// Pull content stream from response
			HttpEntity entity = response.getEntity();
			InputStream inputStream = entity.getContent();

			ByteArrayOutputStream content = new ByteArrayOutputStream();

			// Read response into a buffered stream
			int readBytes = 0;
			while ((readBytes = inputStream.read(sBuffer)) != -1) {
				content.write(sBuffer, 0, readBytes);
			}

			// Return result from buffered stream
			return new String(content.toByteArray());
		} catch (Exception e) {
			throw new ApiException("Problemos jungiantis prie serverio", e);
		}
	}
}
