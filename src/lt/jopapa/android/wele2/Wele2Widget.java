/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lt.jopapa.android.wele2;

import lt.jopapa.android.wele2.R;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
//import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import android.content.SharedPreferences;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Widget'as
 */
public class Wele2Widget extends AppWidgetProvider {
	public static final String WIDGET_CLICK = "lt.jopapa.android.wele2.WIDGET_CLICK";
	public static final String PREFS_NAME = "Wele2Storage";

	private static void setupPendingIntents(Context context,
			AppWidgetManager appWidgetManager, int[] appWidgetIds,
			RemoteViews updateViews) {
		final int N = appWidgetIds.length;
		for (int i = 0; i < N; i++) {
			Intent clickIntent = new Intent(context, Wele2Widget.class);
			clickIntent.setAction(WIDGET_CLICK);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
					0, clickIntent, 0);
			// RemoteViews updateViews = new
			// RemoteViews(context.getPackageName(), R.layout.widget_layout);
			updateViews.setOnClickPendingIntent(R.id.title, pendingIntent);
			appWidgetManager.updateAppWidget(appWidgetIds[i], updateViews);
		}
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {

		//SharedPreferences storage = context.getSharedPreferences(PREFS_NAME, 0);

		// To prevent any ANR timeouts, we perform the update in a service
		Intent serviceIntent = new Intent(context, UpdateService.class);
		serviceIntent.putExtra(WIDGET_CLICK, true);
		context.startService(serviceIntent);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(WIDGET_CLICK)) {
//			Toast.makeText(context, WIDGET_CLICK, Toast.LENGTH_SHORT).show();
//			Log.d("Wele2Widget", WIDGET_CLICK);
			Intent serviceIntent = new Intent(context, UpdateService.class);
			serviceIntent.putExtra(WIDGET_CLICK, true);
			context.startService(serviceIntent);
		} else {
			super.onReceive(context, intent);
		}
	}

	public static class UpdateService extends Service {
		public void handleOnStart(Intent intent) {
			// Toast.makeText(this, intent.getBooleanExtra(WIDGET_CLICK, false)
			// ? "CL" : "false", Toast.LENGTH_SHORT).show();
			if (intent == null || !intent.hasExtra(WIDGET_CLICK)) {
				return;
			}
			RemoteViews updateViews = null;

			updateViews = new RemoteViews(this.getPackageName(), R.layout.widget_layout);
//			String title = String.format(
//					getString(R.string.widget_result),
//					getString(R.string.loading), getString(R.string.loading));
			String title = getString(R.string.loading);
			updateViews.setTextViewText(R.id.title, title);

			ComponentName thisWidget = new ComponentName(this, Wele2Widget.class);
			AppWidgetManager manager = AppWidgetManager.getInstance(this);
			manager.updateAppWidget(thisWidget, updateViews);

			// Build the widget update
			updateViews = buildUpdate(this);

			// Push update for this widget to the home screen
			manager.updateAppWidget(thisWidget, updateViews);

			setupPendingIntents(this, manager,
					manager.getAppWidgetIds(thisWidget), updateViews);
			
			stopSelf();
		}

		// pre 2.0 API, deprecated
		@Override
		public void onStart(Intent intent, int startId) {
			handleOnStart(intent);
		}

		// post 2.0 API
		@Override
		public int onStartCommand(Intent intent, int flags, int startId) {
			onStart(intent, startId);
			return START_STICKY;
		}

		/**
		 * Build a widget update Will block until the online API returns.
		 */
		public RemoteViews buildUpdate(Context context) {
			RemoteViews updateViews = null;

			String pageContent = "";

			try {
				Wele2Helper.prepareUserAgent(context);
				pageContent = Wele2Helper.getPageContent(getString(R.string.wap_url));
			} catch (Exception e) {
				Log.e("Wele2Widget", "Negaliu prisijungti", e);
			}

			SharedPreferences storage = context.getSharedPreferences(PREFS_NAME, 0);
			String dataUsed = storage.getString("dataUsed", "");

			SharedPreferences.Editor editor = storage.edit();
			Pattern pattern = null;
			Matcher matcher = null;
			
			String toastMessage = getString(R.string.reload_failed);
			if (!pageContent.equals("")) {
				pattern = Pattern.compile(getString(R.string.regex_data));
				matcher = pattern.matcher(pageContent);
				if (matcher.find() && matcher.groupCount() == 1) {

					dataUsed = matcher.group(1).replaceAll("&nbsp;", " ").trim();
					
					editor.putString("dataUsed", dataUsed);

					toastMessage = getString(R.string.reload_success);
				}
			}
			
			// amount
			try {
				Wele2Helper.prepareUserAgent(context);
				pageContent = Wele2Helper.getPageContent(getString(R.string.wap_invoice_url));
			} catch (Exception e) {
				Log.e("Wele2Widget", "Negaliu prisijungti", e);
			}

			String amount = storage.getString("amount", "");
			String current_amount = storage.getString("current_amount", "");
//			String period = storage.getString("period", "");

			toastMessage = getString(R.string.reload_failed);
			if (!pageContent.equals("")) {
				pattern = Pattern.compile(getString(R.string.regex_amount));
				matcher = pattern.matcher(pageContent);
				if (matcher.find() && matcher.groupCount() == 1) {

					amount = matcher.group(1).trim();
					
					editor.putString("amount", amount);

					toastMessage = getString(R.string.reload_success);
				}
				pattern = Pattern.compile(getString(R.string.regex_current_amount));
				matcher = pattern.matcher(pageContent);
				if (matcher.find() && matcher.groupCount() == 1) {

					current_amount = matcher.group(1).trim();
					
					editor.putString("current_amount", current_amount);

					toastMessage = getString(R.string.reload_success);
				}
//				pattern = Pattern.compile(getString(R.string.regex_period));
//				matcher = pattern.matcher(pageContent);
//				if (matcher.find() && matcher.groupCount() == 1) {
//
//					period = matcher.group(1).trim();
//					
//					String[] dates = period.split(" - ");
//					String[] fromParts = dates[0].split("\\.");
//					String[] toParts = dates[1].split("\\.");
//					
////					period = dates[0] + " - " + dates[1];
//					period = fromParts[2] + "." + fromParts[1] + "." + fromParts[0] + " - " + toParts[2] + "." + toParts[1] + "." + toParts[0];
//					
//					editor.putString("period", period);
//
//					toastMessage = getString(R.string.reload_success);
//				}
			}
			
			editor.commit();
			
			Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();

			// Build an update that holds the updated widget contents
			updateViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
			String title = String.format(this.getString(R.string.widget_result), amount, current_amount, dataUsed);
			updateViews.setTextViewText(R.id.title, title);

			return updateViews;
		}

		@Override
		public IBinder onBind(Intent intent) {
			// We don't need to bind to this service
			return null;
		}
	}
}
